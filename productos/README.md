Productos Service
This service can show all list of products, create update and delete

to run this service you need installed java version 11 and maven

to execute this service we need to execut the file run_products_service.sh founded in the root of this folders
or you can execute the command mvn spring-boot:run

on the postman folder there is a collection to test this service

to test this service you be sure the service users is running because this service consumes that

you can edit the products if you are an root user or admin user

all changes or retrieves there are saved in the history table

you can see all the produts or any product if you are any user

you can see all the history with the endpoint /products-history

this service runs on the port 8081

#Endpoints method action

- /products get  -> get all products
- /products/product get  -> get product byID
- /products/product put  -> update product
- /products/product post  -> create product
- /products/product delete  -> delete product

- /products-history get -> get all history