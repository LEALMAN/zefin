package com.zefin.emmanuel.test.products.service.impl;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zefin.emmanuel.test.products.dao.ProductHistoryDao;
import com.zefin.emmanuel.test.products.dao.ProductsDao;
import com.zefin.emmanuel.test.products.dto.ProductsDto;
import com.zefin.emmanuel.test.products.dto.ResponseDTO;
import com.zefin.emmanuel.test.products.dto.users.UsersDto;
import com.zefin.emmanuel.test.products.model.ProductHistory;
import com.zefin.emmanuel.test.products.model.Products;
import com.zefin.emmanuel.test.products.model.users.Users;
import com.zefin.emmanuel.test.products.service.ProductsService;
import com.zefin.emmanuel.test.products.util.QueryAction;
import com.zefin.emmanuel.test.products.util.RolesEnum;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProductsServiceImpl implements ProductsService {

	@Autowired
	private ProductsDao productsDao;

	@Autowired
	private ProductHistoryDao productHistoryDao;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private ResponseDTO response;

	@Autowired
	private RestTemplate restTemplate;

	@Value("${user.service.url}")
	private String userServieUrl;

	@Autowired
	private Gson gson;

	@Override
	public ResponseDTO getAllProducts() {
		try {
			var products = this.productsDao.findAll();
			products.removeIf(product -> product.getProductId().equals("0000"));
			this.updateHistoryProduct(new ProductsDto(), QueryAction.RETRIEVE_ALL.toString());
			return this.validateSizeProductsList(products);
		} catch (Exception e) {
			return this.createResponse(null, "400", "Error to get product list ");
		}
	}

	private ResponseDTO validateSizeProductsList(List<Products> productList) {
		if (productList.isEmpty()) {
			return this.createResponse(null, "200", "product list not founded");
		} else {
			List<ProductsDto> productListDto = new ArrayList<ProductsDto>();
			productList.forEach(product -> {
				productListDto.add(modelMapper.map(product, ProductsDto.class));
			});
			return this.createResponse(productListDto, "200", "product list founded");
		}
	}

	@Override
	public ResponseDTO getProductById(ProductsDto productRequest) {
		try {
			if (!StringUtils.isEmpty(productRequest.getProductId())) {
				var product = this.productsDao.findById(productRequest.getProductId());
				var productResponse = modelMapper.map(product, ProductsDto.class);
				this.updateHistoryProduct(productRequest, QueryAction.RETRIEVE.toString());
				return this.createResponse(productResponse, "200", "success to get product");
			} else {
				return this.createResponse(productRequest, "200", "product not founded or invalid request");
			}

		} catch (Exception e) {
			log.error(e.toString());
			return this.createResponse(null, "400", "Error to get product ");
		}
	}

	@Override
	public ResponseDTO saveProduct(ProductsDto productRequest) {
		try {
			Products productModel = modelMapper.map(productRequest, Products.class);
			var user = this.getUserValue(productRequest.getUserId());
			if (this.isValidUserId(user.getUserId()) && this.isRootRole(user.getRoleId())
					|| this.isAdminRole(user.getRoleId())) {
				productModel.setUpdatedAt(Instant.now());
				productModel.setCreatedAt(Instant.now());
				var product = this.productsDao.save(productModel);
				productRequest.setProductId(product.getProductId());
				this.updateHistoryProduct(productRequest, QueryAction.CREATE.toString());
				this.notifyChangesInProductsToAdmins(user.getUserId());
				log.info("product saved {}", product);
				return this.createResponse(product, "200", "Success product saved");
			} else {
				log.info("invalid user to update data {}", productRequest);
				return this.createResponse(null, "400", "error to add Data");
			}
		} catch (Exception e) {
			log.error(e.toString());
			return this.createResponse(null, "400", "Error to save data");
		}
	}

	@Override
	public ResponseDTO updateProduct(ProductsDto productRequest) {
		try {
			Products productModel = modelMapper.map(productRequest, Products.class);
			var user = this.getUserValue(productRequest.getUserId());
			if (this.isValidUserId(user.getUserId()) && this.isRootRole(user.getRoleId())
					|| this.isAdminRole(user.getRoleId())) {
				productModel.setUpdatedAt(Instant.now());
				var product = this.productsDao.save(productModel);
				this.updateHistoryProduct(productRequest, QueryAction.UPDATE.toString());
				this.notifyChangesInProductsToAdmins(user.getUserId());
				log.info("product updated {}", product);
				return this.createResponse(product, "200", "Success product saved");
			} else {
				log.info("invalid user to update data {}", productRequest);
				return this.createResponse(null, "400", "error to modify Data");
			}
		} catch (Exception e) {
			log.error(e.toString());
			return this.createResponse(null, "400", "Error to upate data");
		}
	}

	@Override
	public ResponseDTO deleteProduct(ProductsDto productRequest) {
		try {
			Products productModel = modelMapper.map(productRequest, Products.class);
			var user = this.getUserValue(productRequest.getUserId());
			if (this.isValidUserId(user.getUserId()) && this.isRootRole(user.getRoleId())
					|| this.isAdminRole(user.getRoleId())) {
				productModel.setUpdatedAt(Instant.now());
				this.productsDao.delete(productModel);
				this.updateHistoryProduct(productRequest, QueryAction.DELETE.toString());
				this.notifyChangesInProductsToAdmins(user.getUserId());
				log.info("product delete ");
				return this.createResponse(null, "200", "Success product deleted");
			} else {
				log.info("invalid user to update data {}", productRequest);
				return this.createResponse(productRequest, "400", "error to delete Data");
			}
		} catch (Exception e) {
			log.error(e.toString());
			return this.createResponse(null, "400", "Error to delete data");
		}
	}

	private ResponseDTO createResponse(Object data, String statusCode, String message) {
		this.response.setData(data);
		this.response.setMessage(message);
		this.response.setStatusCode(statusCode);
		return this.response;
	}

	private void updateHistoryProduct(ProductsDto product, String action) {
		var user = this.getUserValue(product.getUserId());
		if (this.isValidUserId(user.getUserId()) && this.isRootRole(user.getRoleId())) {
			var productModel = this.buildHistoryObject(product, action);
			this.productHistoryDao.save(productModel);
			log.info("saved in product history value {}", productModel);
		} else {
			product.setUserId("ANONYMOUS");
			product.setProductId("0000");
			var productModel = this.buildHistoryObject(product, action);
			this.productHistoryDao.save(productModel);
			log.info("saved in product history value {}", productModel);
		}
	}

	private ProductHistory buildHistoryObject(ProductsDto productDto, String action) {
		var user = new Users();
		var product = new Products();
		var productHistory = new ProductHistory();
		user.setUserId(productDto.getUserId());
		productHistory.setDescriptionQuery(action);
		if (productDto.getProductId() != null) {
			product.setProductId(productDto.getProductId());
			productHistory.setProductId(product);
		}
		productHistory.setQueriedAt(Instant.now());
		productHistory.setUserId(user);
		return productHistory;
	}

	private UsersDto getUserValue(String userId) {
		try {
			log.info("connecting to {}:", this.userServieUrl + "user/" + userId);
			ResponseDTO response = restTemplate.getForObject(this.userServieUrl + "user/" + userId, ResponseDTO.class);
			log.info("Validating User id{}", userId);
			var user = gson.fromJson(response.getData().toString(), UsersDto.class);
			log.info("user value {}", user);
			return user;
		} catch (Exception e) {
			log.error("Error to get user data, verify usersservice");
			return new UsersDto();
		}
	}

	private boolean isAdminRole(String role) {
		return role.equalsIgnoreCase(RolesEnum.ADMIN.getDescription());
	}

	private boolean isRootRole(String role) {
		return role.equalsIgnoreCase(RolesEnum.ROOT.getDescription());
	}

	private boolean isValidUserId(String userId) {
		return (!StringUtils.isBlank(userId));
	}

	/***
	 * in this method we can simulate send the usersList to a new service receives
	 * the list as parameter, this service try to send notifications , this service
	 * can do the send of email and sms or push notifications
	 * 
	 **/
	private void notifyChangesInProductsToAdmins(String userID) {
		var usersList = this.getAllUserValue();
		usersList.removeIf(user -> (user.getUserId().equalsIgnoreCase(userID)
				|| !this.isAdminRole(user.getRoleId()) && !this.isRootRole(user.getRoleId())));

		log.info("Users list {}", usersList);
	}

	private List<UsersDto> getAllUserValue() {
		try {
			log.info("connecting to {}:", this.userServieUrl);
			ResponseDTO response = restTemplate.getForObject(this.userServieUrl, ResponseDTO.class);
			log.info("Validating User id{}", response.getData());
			List<UsersDto> userList = gson.fromJson(response.getData().toString(), new TypeToken<List<UsersDto>>() {
			}.getType());
			log.info("user value {}", userList);
			return userList;
		} catch (Exception e) {
			log.error("Error to get user data, verify usersservice");
			return new ArrayList<UsersDto>();
		}
	}

}
