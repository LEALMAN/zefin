package com.zefin.emmanuel.test.products.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.zefin.emmanuel.test.products.model.ProductHistory;

public interface ProductHistoryDao extends CrudRepository<ProductHistory, String>{

	List<ProductHistory> findAll();
}
