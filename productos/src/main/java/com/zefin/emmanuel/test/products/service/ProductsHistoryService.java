package com.zefin.emmanuel.test.products.service;

import com.zefin.emmanuel.test.products.dto.ResponseDTO;

public interface ProductsHistoryService {

	ResponseDTO getAllProductsHistory();
}
