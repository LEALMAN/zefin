package com.zefin.emmanuel.test.products.model.users;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Roles {

	@Id
	@Column(name = "role_id")
	private String roleId;
	private String role;
}
