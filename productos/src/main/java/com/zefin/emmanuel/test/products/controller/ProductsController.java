package com.zefin.emmanuel.test.products.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zefin.emmanuel.test.products.dto.ProductsDto;
import com.zefin.emmanuel.test.products.dto.ResponseDTO;
import com.zefin.emmanuel.test.products.service.ProductsService;

@RestController
@RequestMapping("/products")
public class ProductsController {

	@Autowired
	private ProductsService productService;

	@GetMapping
	public ResponseDTO getAllProducts() {
		return productService.getAllProducts();
	};

	@PostMapping("/product")
	public ResponseDTO saveProduct(@RequestBody ProductsDto product) {
		return this.productService.saveProduct(product);
	};

	@PutMapping("/product")
	public ResponseDTO updateProduct(@RequestBody ProductsDto product) {
		return this.productService.updateProduct(product);
	};

	@GetMapping("/product")
	public ResponseDTO getProductById(@RequestBody ProductsDto product) {
		return this.productService.getProductById(product);
	};

	@DeleteMapping("/product")
	public ResponseDTO deletProduct(@RequestBody ProductsDto product) {
		return this.productService.deleteProduct(product);
	};

}
