package com.zefin.emmanuel.test.products.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zefin.emmanuel.test.products.dao.ProductHistoryDao;
import com.zefin.emmanuel.test.products.dto.ProductHistoryDto;
import com.zefin.emmanuel.test.products.dto.ResponseDTO;
import com.zefin.emmanuel.test.products.model.ProductHistory;
import com.zefin.emmanuel.test.products.service.ProductsHistoryService;

@Service
public class ProductsHistoryServiceImpl implements ProductsHistoryService {
	@Autowired
	private ProductHistoryDao productHistoryDao;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private ResponseDTO response;

	@Override
	public ResponseDTO getAllProductsHistory() {
		try {
			var productsHistory = this.productHistoryDao.findAll();
			return this.validateSizeProductsHistoryList(productsHistory);
		} catch (Exception e) {
			return this.createResponse(null, "400", "Error to get product history list ");
		}
	}

	private ResponseDTO validateSizeProductsHistoryList(List<ProductHistory> productsHistory) {
		if (productsHistory.isEmpty()) {
			return this.createResponse(null, "200", "product history list not founded");
		} else {
			List<ProductHistoryDto> productListDto = new ArrayList<ProductHistoryDto>();
			productsHistory.forEach(product -> {
				productListDto.add(modelMapper.map(product, ProductHistoryDto.class));
			});
			return this.createResponse(productListDto, "200", "product history list founded");
		}
	}

	private ResponseDTO createResponse(Object data, String statusCode, String message) {
		this.response.setData(data);
		this.response.setMessage(message);
		this.response.setStatusCode(statusCode);
		return this.response;
	}
}
