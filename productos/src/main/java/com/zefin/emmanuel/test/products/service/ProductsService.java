package com.zefin.emmanuel.test.products.service;

import com.zefin.emmanuel.test.products.dto.ProductsDto;
import com.zefin.emmanuel.test.products.dto.ResponseDTO;

public interface ProductsService {

	ResponseDTO getAllProducts();

	ResponseDTO saveProduct(ProductsDto productRequest);

	ResponseDTO deleteProduct(ProductsDto productRequest);

	ResponseDTO getProductById(ProductsDto productRequest);

	ResponseDTO updateProduct(ProductsDto productRequest);
}
