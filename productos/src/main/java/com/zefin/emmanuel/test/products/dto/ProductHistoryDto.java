package com.zefin.emmanuel.test.products.dto;

import java.time.Instant;

import com.zefin.emmanuel.test.products.dto.users.UsersDto;

import lombok.Data;

@Data
public class ProductHistoryDto {

	private String productHistoryId;
	private ProductsDto productId;
	private String descriptionQuery;
	private UsersDto queryByUserId;
	private Instant queriedAt;
}
