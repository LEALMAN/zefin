package com.zefin.emmanuel.test.products.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.zefin.emmanuel.test.products.model.Products;

public interface ProductsDao extends CrudRepository<Products, String> {

	List<Products> findAll();
}
