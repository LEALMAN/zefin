package com.zefin.emmanuel.test.products.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class ProductsDto {
	private String productId;

	private String description;

	private BigDecimal price;

	private String userId;
	
	private String brand;
}
