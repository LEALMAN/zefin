package com.zefin.emmanuel.test.products.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zefin.emmanuel.test.products.dto.ResponseDTO;
import com.zefin.emmanuel.test.products.service.ProductsHistoryService;

@RestController
@RequestMapping("/products-history")
public class ProductsHistoryController {

	@Autowired
	private ProductsHistoryService productHistoryService;

	@GetMapping
	public ResponseDTO getAllProducts() {
		return productHistoryService.getAllProductsHistory();
	};
}
