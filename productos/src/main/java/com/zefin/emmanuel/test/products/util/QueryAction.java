package com.zefin.emmanuel.test.products.util;

public enum QueryAction {

	UPDATE, DELETE, RETRIEVE, CREATE, RETRIEVE_ALL;
}
