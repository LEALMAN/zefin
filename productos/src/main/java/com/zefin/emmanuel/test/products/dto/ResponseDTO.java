package com.zefin.emmanuel.test.products.dto;

import lombok.Data;

@Data
public class ResponseDTO {

	private String message;
	private String statusCode;
	private Object data;
}
