package com.zefin.emmanuel.test.products.dto.users;

import lombok.Data;

@Data
public class UsersDto {
	private String userId;
	private String name;
	private String lastName;
	private String secondLastName;
	private String roleId;
	private boolean isVerified;
	private String email;
	private String password;
}
