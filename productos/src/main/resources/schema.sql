DROP TABLE IF EXISTS product_history;
DROP TABLE IF EXISTS products;
  

CREATE TABLE products (
  product_id VARCHAR(250) NOT NULL PRIMARY KEY,
  description VARCHAR(250) NOT NULL,
  brand VARCHAR(250) NOT NULL,
  created_at timestamp ,
  updated_at timestamp ,
  price DOUBLE NOT NULL
);

CREATE TABLE product_history (
  product_history_id VARCHAR(250) NOT NULL PRIMARY KEY,
  product_id VARCHAR(250) NOT NULL,
  description_query VARCHAR(250) NOT NULL,
  user_id VARCHAR(250) NOT NULL,
  queried_at timestamp NOT NULL,
  FOREIGN KEY (product_id) REFERENCES products(product_id),
  FOREIGN KEY (user_id) REFERENCES users(user_id)
);

