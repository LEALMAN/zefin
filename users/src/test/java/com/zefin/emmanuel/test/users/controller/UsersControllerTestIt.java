package com.zefin.emmanuel.test.users.controller;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.zefin.emmanuel.test.users.App;
import com.zefin.emmanuel.test.users.util.CreateObject;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { App.class })
@TestPropertySource(locations = "classpath:application.properties")
public class UsersControllerTestIt {

	@Autowired
	private UsersController usersController;

	@Test
	public void createUser() {
		var user = CreateObject.userDto("some-id", "test", "test@test.com", "test", "last-name-test",
				"secons-last-name-test", "1", "33a480ab-7ec4-4ff0-8b83-434742qas21d");
		var response = usersController.saveUser(user);
		assertNotNull(response);
	}

	@Test
	public void updateUser() {
		var user = CreateObject.userDto("some-id", "test", "test@test.com", "test", "last-name-test",
				"secons-last-name-test", "1", "33a480ab-7ec4-4ff0-8b83-434742qas21d");
		var response = usersController.updateUser(user);
		assertNotNull(response);
	}

	@Test
	public void getAllUser() {
		var response = usersController.getAllUsers();
		assertNotNull(response);
	}

	@Test
	public void deleteUser() {
		var user = CreateObject.userDto("some-id", "test", "test@test.com", "test", "last-name-test",
				"secons-last-name-test", "1", "33a480ab-7ec4-4ff0-8b83-434742qas21d");
		var response = usersController.deletUser(user);
		assertNotNull(response);
	}

}
