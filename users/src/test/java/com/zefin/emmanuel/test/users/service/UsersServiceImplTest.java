package com.zefin.emmanuel.test.users.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.junit.MockitoJUnitRunner;
import org.modelmapper.ModelMapper;

import com.zefin.emmanuel.test.users.dao.UsersDao;
import com.zefin.emmanuel.test.users.dto.ResponseDTO;
import com.zefin.emmanuel.test.users.model.Users;
import com.zefin.emmanuel.test.users.service.impl.UsersServiceImpl;
import com.zefin.emmanuel.test.users.util.CreateObject;

@RunWith(MockitoJUnitRunner.class)
public class UsersServiceImplTest {

	@InjectMocks
	private UsersServiceImpl usersService;

	@Mock
	private UsersDao usersDao;

	private ModelMapper modelMapper;

	private ResponseDTO response;

	@Before
	public void init() {
		modelMapper = new ModelMapper();
		response = new ResponseDTO();
		Whitebox.setInternalState(usersService, "usersDao", usersDao);
		Whitebox.setInternalState(usersService, "modelMapper", modelMapper);
		Whitebox.setInternalState(usersService, "response", response);
	}

	@Test
	public void getAllUsers() {
		var role = CreateObject.role("0", "root");
		var user = CreateObject.user("1", "test", "test@test.com", "test", "last-name-test", "secons-last-name-test",
				role);
		var user1 = CreateObject.user("2", "test1", "test@test.com", "test", "last-name-test", "secons-last-name-test",
				role);
		var usersList = new ArrayList<Users>();
		usersList.add(user);
		usersList.add(user1);
		Mockito.when(this.usersDao.findAll()).thenReturn(usersList);
		var response = this.usersService.getAllUSers();
		assertNotNull(response);
		assertNotNull(response.getData());
		assertNotNull(response.getStatusCode());
		assertEquals(response.getStatusCode(), "200");
	}

	@Test
	public void getUserById() {
		var role = CreateObject.role("0", "root");
		var user = CreateObject.user("1", "test", "test@test.com", "test", "last-name-test", "secons-last-name-test",
				role);
		Mockito.when(this.usersDao.findById("2")).thenReturn(Optional.of(user));
		var response = this.usersService.getUserById("2");
		assertNotNull(response);
		assertNotNull(response.getData());
		assertNotNull(response.getStatusCode());
		assertEquals(response.getStatusCode(), "200");
		assertEquals(response.getMessage(), "success to get user");
	}

	@Test
	public void getUserByIdNotFounded() {
		Mockito.when(this.usersDao.findById("2")).thenReturn(Optional.empty());
		var response = this.usersService.getUserById("2");
		assertNotNull(response);
		assertNull(response.getData());
		assertNotNull(response.getStatusCode());
		assertEquals(response.getStatusCode(), "200");
		assertEquals(response.getMessage(), "user not founded verify data");
	}

	@Test
	public void createUser() {
		var role = CreateObject.role("0", "root");
		var user = CreateObject.user("1", "test", "test@test.com", "test", "last-name-test", "secons-last-name-test",
				role);
		var userDto = CreateObject.userDto("some-id", "test", "test@test.com", "test", "last-name-test",
				"secons-last-name-test", "1", "33a480ab-7ec4-4ff0-8b83-434742qas21d");
		Mockito.when(this.usersDao.findById(userDto.getLoggedUserId())).thenReturn(Optional.of(user));
		Mockito.when(this.usersDao.save(Mockito.any())).thenReturn(user);
		var response = this.usersService.saveUser(userDto);
		assertNotNull(response);
		assertNotNull(response.getData());
		assertNotNull(response.getStatusCode());
		assertEquals(response.getStatusCode(), "200");
		assertEquals(response.getMessage(), "Success user saved");
	}

	@Test
	public void createUserInvalidData() {
		var userDto = CreateObject.userDto("some-id", "test", "test@test.com", "test", "last-name-test",
				"secons-last-name-test", "1", "33a480ab-7ec4-4ff0-8b83-434742qas21d");
		Mockito.when(this.usersDao.findById(userDto.getLoggedUserId())).thenReturn(Optional.empty());
		var response = this.usersService.saveUser(userDto);
		assertNotNull(response);
		assertNull(response.getData());
		assertNotNull(response.getStatusCode());
		assertEquals(response.getStatusCode(), "200");
		assertEquals(response.getMessage(), "error to save data, invalid user");
	}

	@Test
	public void updateUser() {
		var role = CreateObject.role("0", "root");
		var user = CreateObject.user("1", "test", "test@test.com", "test", "last-name-test", "secons-last-name-test",
				role);
		var userDto = CreateObject.userDto("some-id", "test", "test@test.com", "test", "last-name-test",
				"secons-last-name-test", "1", "33a480ab-7ec4-4ff0-8b83-434742qas21d");
		Mockito.when(this.usersDao.findById(userDto.getLoggedUserId())).thenReturn(Optional.of(user));
		Mockito.when(this.usersDao.save(Mockito.any())).thenReturn(user);
		var response = this.usersService.updateUser(userDto);
		assertNotNull(response);
		assertNotNull(response.getData());
		assertNotNull(response.getStatusCode());
		assertEquals(response.getStatusCode(), "200");
		assertEquals(response.getMessage(), "Success user updated");
	}

	@Test
	public void updateUserInvalidData() {
		var userDto = CreateObject.userDto("some-id", "test", "test@test.com", "test", "last-name-test",
				"secons-last-name-test", "1", "33a480ab-7ec4-4ff0-8b83-434742qas21d");
		Mockito.when(this.usersDao.findById(userDto.getLoggedUserId())).thenReturn(Optional.empty());
		var response = this.usersService.updateUser(userDto);
		assertNotNull(response);
		assertNull(response.getData());
		assertNotNull(response.getStatusCode());
		assertEquals(response.getStatusCode(), "200");
		assertEquals(response.getMessage(), "error to update data, invalid user");
	}

	@Test
	public void deleteUser() {
		var role = CreateObject.role("0", "root");
		var user = CreateObject.user("1", "test", "test@test.com", "test", "last-name-test", "secons-last-name-test",
				role);
		var userDto = CreateObject.userDto("some-id", "test", "test@test.com", "test", "last-name-test",
				"secons-last-name-test", "1", "33a480ab-7ec4-4ff0-8b83-434742qas21d");
		Mockito.when(this.usersDao.findById(userDto.getLoggedUserId())).thenReturn(Optional.of(user));
		var response = this.usersService.deleteUser(userDto);
		assertNotNull(response);
		assertNull(response.getData());
		assertNotNull(response.getStatusCode());
		assertEquals(response.getStatusCode(), "200");
		assertEquals(response.getMessage(), "Success user deleted");
	}

	@Test
	public void deleteUserInvalidData() {
		var userDto = CreateObject.userDto("some-id", "test", "test@test.com", "test", "last-name-test",
				"secons-last-name-test", "1", "33a480ab-7ec4-4ff0-8b83-434742qas21d");
		Mockito.when(this.usersDao.findById(userDto.getLoggedUserId())).thenReturn(Optional.empty());
		var response = this.usersService.deleteUser(userDto);
		assertNotNull(response);
		assertNull(response.getData());
		assertNotNull(response.getStatusCode());
		assertEquals(response.getStatusCode(), "200");
		assertEquals(response.getMessage(), "error to delete data, invalid user");
	}
}
