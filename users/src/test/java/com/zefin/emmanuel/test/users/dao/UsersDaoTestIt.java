package com.zefin.emmanuel.test.users.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.zefin.emmanuel.test.users.App;
import com.zefin.emmanuel.test.users.util.CreateObject;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { App.class })
@TestPropertySource(locations = "classpath:application.properties")
public class UsersDaoTestIt {

	@Autowired
	private UsersDao userDao;

	@Test
	public void createUserTest() {
		var role = CreateObject.role("0", "root");
		var user = CreateObject.user(null, "test", "test@test.com", "test", "last-name-test", "secons-last-name-test",
				role);
		var usserSaved = this.userDao.save(user);
		assertNotNull(usserSaved);
		assertNotNull(usserSaved.getUserId());
		assertEquals("test@test.com", usserSaved.getEmail());

	}

	@Test
	public void findUserTest() {
		var usserSaved = this.userDao.findById("ANONYMOUS").get();
		assertNotNull(usserSaved);
		assertEquals("xxxx", usserSaved.getEmail());
		assertEquals("ANONYMOUS", usserSaved.getUserId());
	}

	@Test
	public void findAllUserTest() {
		var usserSaved = this.userDao.findAll();
		assertNotNull(usserSaved);
		assertTrue(usserSaved.size() > 0);
	}
}
