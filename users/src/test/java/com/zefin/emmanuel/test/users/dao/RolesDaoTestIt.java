package com.zefin.emmanuel.test.users.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import com.zefin.emmanuel.test.users.App;
import com.zefin.emmanuel.test.users.util.CreateObject;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { App.class })
@TestPropertySource(locations = "classpath:application.properties")
public class RolesDaoTestIt {

	@Autowired
	private RolesDao rolesDao;

	@Test
	public void createRoleTest() {
		var role = CreateObject.role("5", "test");
		var roledSaved = this.rolesDao.save(role);
		assertNotNull(roledSaved);
		assertEquals("test", roledSaved.getRole());
		assertEquals("5", roledSaved.getRoleId());
	}

	@Test
	public void createFindRoleTest() {
		var roledSaved = this.rolesDao.findById("0").get();
		assertNotNull(roledSaved);
		assertEquals("root", roledSaved.getRole());
		assertEquals("0", roledSaved.getRoleId());
	}
}
