package com.zefin.emmanuel.test.users.util;

import com.zefin.emmanuel.test.users.dto.UsersDto;
import com.zefin.emmanuel.test.users.model.Roles;
import com.zefin.emmanuel.test.users.model.Users;

public class CreateObject {

	public static UsersDto userDto(String userId, String name, String email, String password, String lastName,
			String secondLastName, String roleId, String loggedUserId) {

		return UsersDto.builder().userId(userId).name(name).email(email).password(password).lastName(lastName)
				.secondLastName(secondLastName).roleId(roleId).loggedUserId(loggedUserId).build();
	}

	public static Users user(String userId, String name, String email, String password, String lastName,
			String secondLastName, Roles roleId) {

		return Users.builder().userId(userId).name(name).email(email).password(password).lastName(lastName)
				.secondLastName(secondLastName).roleId(roleId).isVerified(true).build();
	}

	public static Roles role(String roleId, String role) {
		return Roles.builder().role(role).roleId(roleId).build();
	}

}
