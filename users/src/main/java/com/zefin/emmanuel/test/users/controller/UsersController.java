package com.zefin.emmanuel.test.users.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.zefin.emmanuel.test.users.dto.ResponseDTO;
import com.zefin.emmanuel.test.users.dto.UsersDto;
import com.zefin.emmanuel.test.users.service.UsersService;

@RestController
@RequestMapping("/users")
public class UsersController {

	@Autowired
	private UsersService usersService;

	@GetMapping
	public ResponseDTO getAllUsers() {
		return usersService.getAllUSers();
	};

	@PostMapping("/user")
	public ResponseDTO saveUser(@RequestBody UsersDto user) {
		return this.usersService.saveUser(user);
	};
	
	@PutMapping("/user")
	public ResponseDTO updateUser(@RequestBody UsersDto user) {
		return this.usersService.updateUser(user);
	};

	@GetMapping("/user/{userId}")
	public ResponseDTO ResponseDTOgetUserById(@PathVariable String userId) {
		return this.usersService.getUserById(userId);
	};

	@DeleteMapping("/user")
	public ResponseDTO deletUser(@RequestBody UsersDto user) {
		return this.usersService.deleteUser(user);
	};

}
