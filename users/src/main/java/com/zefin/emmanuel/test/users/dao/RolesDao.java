package com.zefin.emmanuel.test.users.dao;

import org.springframework.data.repository.CrudRepository;

import com.zefin.emmanuel.test.users.model.Roles;

public interface RolesDao extends CrudRepository<Roles, String>{

}
