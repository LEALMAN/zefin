package com.zefin.emmanuel.test.users.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.zefin.emmanuel.test.users.model.Users;

public interface UsersDao extends CrudRepository<Users, String> {

	List<Users> findAll();

}
