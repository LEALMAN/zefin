package com.zefin.emmanuel.test.users.service.impl;

public enum RolesEnum {

	ROOT("0", "ROOT"), ADMIN("1", "ADMIN"), USER("2", "USER"), NOT_USER("3", "NOT_USER");

	private String value;
	private String description;

	private RolesEnum(String value, String description) {
		this.value = value;
		this.description = description;
	}

	public String getValue() {
		return value;
	}

	public String getDescription() {
		return description;
	}

}
