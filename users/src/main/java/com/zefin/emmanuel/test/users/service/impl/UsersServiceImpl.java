package com.zefin.emmanuel.test.users.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zefin.emmanuel.test.users.dao.UsersDao;
import com.zefin.emmanuel.test.users.dto.ResponseDTO;
import com.zefin.emmanuel.test.users.dto.UsersDto;
import com.zefin.emmanuel.test.users.model.Users;
import com.zefin.emmanuel.test.users.service.UsersService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UsersServiceImpl implements UsersService {

	@Autowired
	private UsersDao usersDao;

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private ResponseDTO response;

	@Override
	public ResponseDTO getAllUSers() {
		try {
			var users = this.usersDao.findAll();
			return this.validateSizeUserList(users);
		} catch (Exception e) {
			return this.createResponse(null, "400", "Error to get user list ");
		}
	}

	private ResponseDTO validateSizeUserList(List<Users> usersList) {
		if (usersList.isEmpty()) {
			return this.createResponse(null, "200", "user list not founded");
		} else {
			List<UsersDto> usersListDto = new ArrayList<UsersDto>();
			usersList.forEach(user -> {
				usersListDto.add(modelMapper.map(user, UsersDto.class));
			});
			return this.createResponse(usersListDto, "200", "user list founded");
		}
	}

	@Override
	public ResponseDTO getUserById(String userIdRequest) {
		try {
			var user = this.usersDao.findById(userIdRequest);
			if (!user.isEmpty()) {
				var userResponse = modelMapper.map(user.get(), UsersDto.class);
				return this.createResponse(userResponse, "200", "success to get user");
			} else {
				log.warn("Error to get user id: {} ",userIdRequest);
				return this.createResponse(null, "200", "user not founded verify data");
			}

		} catch (Exception e) {
			log.error("Error to get user id: {} message {}", userIdRequest, e);
			return this.createResponse(null, "400", "Error to get user");
		}
	}

	@Override
	public ResponseDTO saveUser(UsersDto userRequest) {
		try {
			Users userTosave = modelMapper.map(userRequest, Users.class);
			var user = this.usersDao.findById(userRequest.getLoggedUserId());
			if (!user.isEmpty() && this.isRootRole(user.get().getRoleId().getRole())) {
				var userResponse = this.usersDao.save(userTosave);
				log.info("user saved {}", userResponse);
				return this.createResponse(userResponse, "200", "Success user saved");
			} else {
				log.warn("error to save data, invalid user", userRequest);
				return this.createResponse(null, "200", "error to save data, invalid user");
			}
		} catch (Exception e) {
			log.error(e.toString());
			return this.createResponse(null, "400", "Error to save data");
		}
	}

	@Override
	public ResponseDTO updateUser(UsersDto userRequest) {
		try {
			Users userModel = modelMapper.map(userRequest, Users.class);
			var user = this.usersDao.findById(userRequest.getLoggedUserId());
			if (!user.isEmpty() && this.isRootRole(user.get().getRoleId().getRole())) {
				var userResponse = this.usersDao.save(userModel);
				log.info("user updated {}", userResponse);
				return this.createResponse(userResponse, "200", "Success user updated");
			} else {
				log.warn("error to save data, invalid user", userRequest);
				return this.createResponse(null, "200", "error to update data, invalid user");
			}
		} catch (Exception e) {
			log.error(e.toString());
			return this.createResponse(null, "400", "Error to update data");
		}
	}

	@Override
	public ResponseDTO deleteUser(UsersDto userRequest) {
		try {
			Users userModel = modelMapper.map(userRequest, Users.class);
			var user = this.usersDao.findById(userRequest.getLoggedUserId());
			if (!user.isEmpty() && this.isRootRole(user.get().getRoleId().getRole())) {
				this.usersDao.delete(userModel);
				return this.createResponse(null, "200", "Success user deleted");
			} else {
				log.warn("error to save data, invalid user", userRequest);
				return this.createResponse(null, "200", "error to delete data, invalid user");
			}
		} catch (Exception e) {
			log.error(e.toString());
			return this.createResponse(null, "400", "Error to delete data");
		}
	}

	private ResponseDTO createResponse(Object data, String statusCode, String message) {
		this.response.setData(data);
		this.response.setMessage(message);
		this.response.setStatusCode(statusCode);
		return this.response;
	}

	private boolean isRootRole(String role) {
		return role.equalsIgnoreCase(RolesEnum.ROOT.getDescription());
	}

}
