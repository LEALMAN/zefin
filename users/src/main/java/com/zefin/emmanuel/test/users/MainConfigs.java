package com.zefin.emmanuel.test.users;

import java.sql.SQLException;

import org.h2.tools.Server;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zefin.emmanuel.test.users.dto.ResponseDTO;

@Configuration
public class MainConfigs {

	@Bean
	public static ModelMapper modelMapper() {
		return new ModelMapper();
	}

	@Bean
	public static ResponseDTO response() {
		return new ResponseDTO();
	}
	
	@Bean(initMethod = "start", destroyMethod = "stop")
	public Server inMemoryH2DatabaseaServer() throws SQLException {
	    return Server.createTcpServer(
	      "-tcp", "-tcpAllowOthers", "-tcpPort", "9090");
	}

}
