package com.zefin.emmanuel.test.users.dto;

import lombok.Data;

@Data
public class RolesDto {

	private String roleId;
	private String role;

}
