package com.zefin.emmanuel.test.users.service;

import com.zefin.emmanuel.test.users.dto.ResponseDTO;
import com.zefin.emmanuel.test.users.dto.UsersDto;

public interface UsersService {

	ResponseDTO getAllUSers();

	ResponseDTO saveUser(UsersDto userRequest);
	
	ResponseDTO updateUser(UsersDto userRequest);

	ResponseDTO deleteUser(UsersDto userRequest);

	ResponseDTO getUserById(String userIdRequest);
}
