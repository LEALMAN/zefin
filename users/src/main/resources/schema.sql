DROP TABLE IF EXISTS roles;
DROP TABLE IF EXISTS users;
  
CREATE TABLE roles (
  role_Id VARCHAR(250) NOT NULL PRIMARY KEY,
  role VARCHAR(250) NOT NULL
);


CREATE TABLE users (
  user_id VARCHAR(250) NOT NULL PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,
  second_last_name VARCHAR(250) NOT NULL,
  is_verified boolean NOT NULL,
  password VARCHAR(250) NOT NULL,
  email VARCHAR(250) NOT NULL ,
  role_id VARCHAR(250) NOT NULL ,
  FOREIGN KEY (role_id) REFERENCES roles(role_id)
);

