# Users Service
This service can show all list of users, create update and delete

to run this service you need installed java version 11 and maven

to execute this service we need to execute the file run_users_service.sh founded in the root of this folders
or you can execute the command mvn spring-boot:run

to run unit test execute the command mvn clean install verify -DiTests 

on the postman folder there is a collection to test this service

this service enable the products service to test

you can edit the users if you are an root user 

you can see all the users 

this service runs on the port 8082

#Endpoints method action

- /users get  -> get all usera
- /users/user get  -> get user byID
- /users/user put  -> update user
- /users/user post  -> create user
- /users/user delete  -> delete user
